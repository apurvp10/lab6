/*
	Name: Apurv Patel
	Username: apurvp
	Lab5
	Lab Section: 1021-002
	Name of TA: Hollis and Emily
*/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  
  //This is to seed the random generator 
  srand(unsigned (time(0)));

  //Iniatlizing the deck of 52 cards

	Card deck[52];
	
	int i,j;

	for(i=0;i<4;i++){

		for(j=0;j<13;j++){

			deck[13*i+j].suit = static_cast <Suit> (i);		

			deck[13*i+j].value = (j+2);

		}

	}

  // Shuffles the deck

	random_shuffle(&deck[0],&deck[52],myrandom);

	//Declaring a card hand and initializing it with first five card of the deck

	Card hand[5];

		for(i=0;i<5;i++){

			hand[i].suit=deck[i].suit;

			hand[i].value=deck[i].value;

		}

   //sorting the hand
	sort(hand+0,hand+5,suit_order);

  //printing the hand in sorted order

	for(i=0;i<5;i++){

		cout << setw(10) << get_card_name(hand[i])<<" of "<< get_suit_code(hand[i])<<endl;		

	}

  return 0;

}

//Function to order the hand

bool suit_order(const Card& lhs, const Card& rhs) {
  return (lhs.suit < rhs.suit);
  return (lhs.value < rhs.suit);
}

//This fucntion gets the suit code and returns the symbol for each suit

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

// This Function gets the value
string get_card_name(Card& c) {
  switch (c.value) {
		case 2:		return "2";
		case 3:		return "3";
		case 4:		return "4";
		case 5:		return "5";
		case 6:		return "6";
		case 7:		return "7";
		case 8:		return "8";
		case 9:		return "9";
		case 10:	return "10";
		case 11:	return "Jack";
		case 12:	return "Queen";
		case 13:	return "King";
		case 14:	return "Ace";
		default: 	return "";
	}
}
